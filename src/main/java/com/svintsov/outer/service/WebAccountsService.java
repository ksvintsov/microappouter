package com.svintsov.outer.service;

import com.svintsov.outer.exception.AccountNotFoundException;
import com.svintsov.outer.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

@Service
public class WebAccountsService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${accountservice-url}")
    private String serviceUrl;

    public Account findByAccountId(String accountId) {
        Account account = restTemplate.getForObject(serviceUrl
                + "/account/{id}", Account.class, accountId);

        if (account == null)
            throw new AccountNotFoundException(accountId);
        else
            return account;
    }

    public BigDecimal calculateSummaryBalance() {

        ResponseEntity<List<Account>> responseEntity = restTemplate.exchange(serviceUrl
                + "/account/all", HttpMethod.GET, null, new ParameterizedTypeReference<List<Account>>() {
        });

        List<Account> accounts = responseEntity.getBody();

        return accounts == null ? BigDecimal.ZERO : getTotalBalance(accounts);

    }

    private BigDecimal getTotalBalance(List<Account> accounts) {

        return accounts.stream().map(Account::getBalance).reduce(BigDecimal.ZERO, BigDecimal::add);

    }
}