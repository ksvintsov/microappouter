package com.svintsov.outer.controller;

import com.svintsov.outer.model.Account;
import com.svintsov.outer.service.WebAccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/account")
public class WebAccountsController {

    @Autowired
    protected WebAccountsService accountsService;

    public WebAccountsController(WebAccountsService accountsService) {
        this.accountsService = accountsService;
    }

    @GetMapping("/info/{accountId}")
    public Account getInfoByAccountId(
            @PathVariable("accountId") String accountId) {

        return accountsService.findByAccountId(accountId);
    }

    @GetMapping("/info/summaryBalance")
    public BigDecimal getSummaryBalance(){

        return accountsService.calculateSummaryBalance();

    }

}
