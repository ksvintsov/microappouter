package com.svintsov.outer.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Account {

    private String id;
    private String owner;
    private BigDecimal balance;
}